#pragma once
#include "base_types.h"
class shape
{
public:
	float virtual getArea()=0;
	float virtual getFrameRect() = 0;
	void virtual move() = 0;
};

