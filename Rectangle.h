#pragma once
#include "base_types.h"
#include "shape.h"

class Rectangle: public shape
{
public:
	Rectangle();
	void shape::move();
	float shape::getArea();
	base_types::rectangle_t ob;
	float shape::getFrameRect();
	Rectangle(base_types::rectangle_t ob);
	~Rectangle();
};

